This is a blog created for Web Application Architectures
by Greg Heileman with the University of New Mexico MOOC class.

My local machine only supports Ruby 1.9 so I am using a cloud development environment.

Nitrous.IO enables you to develop web applications completely in the
cloud. This development "box" helps you write software, collaborate
real-time with friends, show off apps to teammates or clients, and
deploy apps to production hosting sites like Heroku or Google App
Engine.